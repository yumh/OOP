package test.uniud.dimi;

import it.uniud.dimi.Poly.NegativeExponentException;
import it.uniud.dimi.Poly.Poly;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by omar on 05/04/17.
 */
public class PolyTest {
    @Test
    void addTest() throws NegativeExponentException {
        Poly a = new Poly(5, 2);
        Poly b = new Poly(7, 2);
        Poly c = a.add(b);
        assertTrue(c.equals(new Poly(12, 2)));

        a = new Poly(7, 6);
        b = new Poly(5, 6);
        c = a.add(b);
        assertTrue(! c.equals(new Poly(12, 2)));
    }

    @Test
    void addShouldBeCommutativeTest() throws NegativeExponentException {
        Poly a = new Poly(5, 2);
        Poly b = new Poly(2, 5);

        Poly c = a.add(b);
        Poly d = b.add(a);

        assertTrue(c.equals(d));
    }

    @Test
    void shouldProduceZeroTest() throws NegativeExponentException {
        Poly a = new Poly(4, 2);
        Poly b = (new Poly(a)).minus();
        Poly zero = new Poly();

        assertTrue(zero.equals(a.add(b)));
    }

    @Test
    void minusTest() throws NegativeExponentException {
        Poly a = new Poly(5, 2);
        Poly b = new Poly(-5,2);

        assertTrue(a.minus().equals(b));
    }

    @Test
    void mulTest() throws NegativeExponentException {
        // calculating a *complex* Poly
        Poly a = new Poly(5, 2);
        Poly b = new Poly(3, 1);
        Poly c = new Poly(4, 4);
        Poly d = new Poly(3, 3);

        Poly x = a.add(b);
        Poly y = c.add(d);

        // here it is, our *complex* Poly
        Poly result = x.mul(y);

        // calculating the expected result
        Poly expectedResult = (new Poly(20, 6))
                .add(new Poly(27, 5))
                .add(new Poly(9, 4));

        assertTrue(expectedResult.equals(result));
    }

    @Test
    void simpleEqualsTest() throws NegativeExponentException {
        Poly a = new Poly(7, 11);
        Poly b = new Poly(7, 11);
        assertTrue(a.equals(b)); // a should be equals to b
        assertTrue(b.equals(a)); // b should be equals to a
        assertTrue(a.equals(a)); // a should be equals to itself

        b = new Poly(a);
        assertTrue(b.equals(a)); // b (a' clone) should be equals to a

        Poly zero = new Poly();
        assertTrue(! zero.equals(b));
    }
}
