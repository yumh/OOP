package test.uniud.dimi;

import it.uniud.dimi.Poly.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by omar on 07/04/17.
 */
public class PolysTest {
    public PolyInterface<Monome> getInstance() {
        return new DensePoly();
    }

    public PolyInterface<Monome> getInstance(PolyInterface<Monome> a) {
        return new DensePoly(a);
    }

    public PolyInterface<Monome> getInstance(int coefficient, int exponent) {
        return new DensePoly(coefficient, exponent);
    }

    @Test
    void constructorTest() {
        assertTrue(getInstance().isZero(), "A new instance should be equals to 0");
        assertEquals(getInstance().degree(), 0, "The degree of the 0 DensePoly should be 0");
        assertEquals(getInstance(3, 7).degree(), 7, "two-int constructor should initialize the poly properly");
    }

    @Test
    void addTest() {
        PolyInterface<Monome> a = getInstance(5, 2);
        PolyInterface<Monome> b = getInstance(7, 2);
        PolyInterface<Monome> c = a.add(b);
        assertEquals(a, getInstance(12, 2));

        a = getInstance(7, 6);
        b = getInstance(5, 6);
        c = a.add(b);
        assertNotEquals(c, getInstance(12, 2));
    }

    @Test
    void shouldProduceZeroTest() {
        PolyInterface<Monome> a = getInstance(4, 2);
        PolyInterface<Monome> b = getInstance(a).minus();
        PolyInterface<Monome> zero = getInstance();
        assertEquals(zero, a.add(b));
    }

    @Test
    void minusTest() {
        PolyInterface<Monome> a = getInstance(5,  2);
        PolyInterface<Monome> b = getInstance(-5, 2);

        assertEquals(a.minus(), b);
    }

    @Test
    void mulTest() {
        // calculating a *complex* Poly
        PolyInterface<Monome> a = getInstance(5, 2);
        PolyInterface<Monome> b = getInstance(3, 1);
        PolyInterface<Monome> c = getInstance(4, 4);
        PolyInterface<Monome> d = getInstance(3, 3);

        PolyInterface<Monome> x = a.add(b);
        PolyInterface<Monome> y = c.add(d);

        PolyInterface<Monome> result = x.mul(y);

        PolyInterface<Monome> expectedResult = getInstance(20, 6)
                .add(getInstance(27, 5))
                .add(getInstance(9,4))
                ;
        System.out.println(result.toString());

        assertEquals(expectedResult, result);
    }
}
