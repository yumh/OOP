package test.uniud.dimi;

import it.uniud.dimi.IntSet.EmptyIntSetException;
import it.uniud.dimi.IntSet.IntBag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by omar on 06/04/17.
 */
class IntBagTest {

    @Test
    void constructorTest() {
        // PolysTest the empty constructor
        IntBag empty = new IntBag();
        assertEquals(empty.size(), 0, "With the empty constructor the bag should contain 0 elements");

        // PolysTest the array constructor
        Integer[] list = new Integer[] {1, 2, 3, 4};
        IntBag notEmpty = new IntBag(list);
        assertEquals(notEmpty.size(), list.length, "With the array constructor the bag should contain the same values that are in the array");

        // TODO: PolysTest the clone constructor
    }

    @Test
    void addTest() {
        Integer a = new Integer(6);
        Integer b = new Integer(6);

        IntBag i = new IntBag();
        assertTrue(i.size() == 0, "A newly created bag should have size 0");

        i.add(a);
        assertTrue(i.size() == 1, "Add to an empty bag should produce a bag with one element");

        i.add(b);
        assertTrue(i.size() == 2, "A bag can contain two equals objects");
    }

    @Test
    void removeTest() {
        final int length = 5;

        Integer[] list = new Integer[5];
        for (int i = 0; i < length; i++) { list[i] = new Integer(i); }

        IntBag i = new IntBag(list);
        assertTrue(i.size() == length, String.format("The size should be %d", length));

        Integer zero = new Integer(0);
        boolean res = i.remove(zero);
        assertTrue(res, "Return should be true if an element has been removed");
        assertTrue(i.size() == length -1, "Once an element has been removed, the size would be decremented");
        assertFalse(i.contains(zero), "The removed element shouldn't still be present");
    }

    @Test
    void removeAllTest() {
        final int lenght = 8;
        Integer zero = 0; // after all, autoboxing is cool!

        IntBag i = new IntBag();

        for (int j = 0; j < lenght; j++) {
            i.add(zero);
        }
        assertTrue(i.size() == lenght, "Bag should store objects that are equals");

        i.removeAll(zero);
        assertTrue(i.size() == 0, "'removeAll' should remove all the element equals to the given");
    }

    @Test
    void containTest() {
        IntBag i = new IntBag();
        Integer a = 5;

        i.add(a);
        assertTrue(i.contains(a), "'contains' should return true if the element is present");
    }

    @Test
    void numberOfCopiesOfTest() {
        Integer zero = 0;
        IntBag i = new IntBag();
        final int length = 5;

        for (int j = 0; j < length; j++) { i.add(zero); }
        assertTrue(i.numberOfCopiesOf(zero) == length, "'numberOfCopiesOf' should return the times an element is present");
    }

    @Test
    void sizeTest() {
        final int length = 6;
        IntBag i = new IntBag();
        Integer zero = 0;

        for (int j = 0; j < length; j++) { i.add(zero); }
        assertTrue(i.size() == length);
    }

    @Test
    void chooseTest() throws EmptyIntSetException {
        IntBag i = new IntBag();
        final int length = 5;
        Integer zero = 0;

        for (int j = 0; j < length; j++) { i.add(zero); }
        assertTrue(i.choose().equals(zero), "Choose should return an element inside the bag");
    }

    @Test
    void chooseShouldThrowExceptionTest() {
        IntBag i = new IntBag();
        assertTrue(i.size() == 0, "Newly created bag should have size 0");

        // trigger the exception
        try {
            i.choose();
            fail("Expected an EmptyIntSetException to be thrown");
        } catch (EmptyIntSetException e) {
            assertTrue(true, "The exception has been catched, gg");
        }
    }
}