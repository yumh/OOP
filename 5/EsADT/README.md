## exercise 1

Complete the implementation of the class `Poly`, then write some tests.


## exercise 2

Starting from the partial implementation of the class `IntSet` write a class
`IntBag` (still using `Vector<Integer>` as data structure).

A *bag*, or multi-set, is a data structure that represent a set that store even
duplicate objects.

Write then some tests.
