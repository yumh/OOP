package it.uniud.dimi.Poly;


import java.util.Objects;

/**
 * Created by omar on 07/04/17.
 */
public class Monome extends AbstractMonome implements Monomial<Integer> {

    /**
     * Will store the exponent
     */
    Integer exponent;

    /**
     * Will store the coefficent
     */
    Integer coefficent;

    /**
     * The 'zero' constructor
     */
    public Monome() {
        this.coefficent = 0;
        this.exponent = 0;
    }

    /**
     * Non-zero contructor
     * @param coefficent the coefficent
     * @param exponent the exponent, REQUIRED >= 0
     */
    public Monome(Integer coefficent, Integer exponent) {
        this.coefficent = coefficent;
        this.exponent = exponent;
    }

    /**
     * The clone contructor
     * @param m a monome to be cloned, REQUIRE not null
     */
    public Monome(Monome m) {
        Objects.requireNonNull(m);

        this.coefficent = m.coefficent;
        this.exponent = m.exponent;
    }

    /**
     * Sum this with m & return a new monome
     * @param m a Monome to be summed with this, REQUIRED not null, REQUIRED to have the same exponent
     * @return a new Monome equals to this + m
     * @throws IllegalArgumentException if this and m does not have the same exponent
     */
    public Monome add(Monomial<Integer> m) {
        Objects.requireNonNull(m);

        if (! this.sameDegree(m)) {
            throw new IllegalArgumentException("this and m does not have the same exponent");
        }

        return new Monome(
                this.getCoefficent() + m.getCoefficent(),
                this.getExponent()
        );
    }

    /**
     * Perform the moltiplication
     * @param m a monomed to be multiplied, REQUIRED not null
     * @return a new monome that is this*m
     */
    public Monome mul(Monomial<Integer> m) {
        Objects.requireNonNull(m);

        return new Monome(
                this.coefficent * m.getCoefficent(),
                this.exponent + m.getExponent()
        );
    }

    /**
     * @return Return a monome that is the negation of this
     */
    public Monome negate() {
        Monome result = new Monome(this);
        result.coefficent *= -1;

        return result;
    }

    public Integer getExponent() {
        return exponent;
    }

    public Integer getCoefficent() {
        return coefficent;
    }

    @Override
    public Monome clone() {
        return new Monome(this);
    }
}
