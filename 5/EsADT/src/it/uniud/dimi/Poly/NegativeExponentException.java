package it.uniud.dimi.Poly;

/**
 * Created by omar on 05/04/17.
 */
public class NegativeExponentException extends Exception {

    public NegativeExponentException() {
        super("The exponent cannot be negative");
    }

}
