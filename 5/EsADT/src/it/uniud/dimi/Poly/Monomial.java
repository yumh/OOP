package it.uniud.dimi.Poly;

/**
 * Created by omar on 07/04/17.
 */
public interface Monomial<N extends Comparable<N>> extends Comparable<Monomial<N>> {

    public boolean isZero();

    public Monomial<N> add(Monomial<N> m);

    public Monomial<N> mul(Monomial<N> m);

    public Monomial<N> negate();

    public N getExponent();

    public N getCoefficent();

    public boolean sameDegree(Monomial<N> m);
}
