package it.uniud.dimi.Poly;

import java.text.DecimalFormat;
import java.util.Objects;

/**
 * Created by omar on 07/04/17.
 */
public abstract class AbstractMonome implements Monomial<Integer> {

    /**
     * @return true if this is zero
     */
    public boolean isZero() {
        return this.getCoefficent().equals(0);
    }

    /**
     * @param O an object to be tested
     * @return true if equals to this, false otherwise
     */
    public boolean equals(Object O) {
        if (O instanceof Monome || O == null) {
            return false;
        }

        Monome o = (Monome) O;

        if (this.isZero() && o.isZero()) {
            return true;
        }

        return this.getExponent().equals(o.getExponent())
                && o.getCoefficent().equals(this.getCoefficent());
    }

    /**
     * @param o a monome to be compared with this. REQUIRE not null
     * @return -1 if this < o, 0 if they are equals, 1 if this > o
     */
    public int compareTo(Monomial<Integer> o) {
        Objects.requireNonNull(o);

        switch (this.getExponent().compareTo(o.getExponent())) {
            case -1:
                return -1;

            case 0:
                return 0;

            default:
                return this.getCoefficent().compareTo(o.getCoefficent()) < 0
                        ? -1
                        :  1;
        }
    }

    public String toString() {
        DecimalFormat fmt = new DecimalFormat("+#0;-#0");
        StringBuffer b = new StringBuffer();
        b.append(fmt.format(getCoefficent()));
        b.append(String.format("*x^%d", getExponent()));
        return b.toString();
    }

    public boolean sameDegree(Monomial<Integer> m) {
        return this.getExponent().compareTo(m.getExponent()) == 0;
    }
}
