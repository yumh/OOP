package it.uniud.dimi.Poly;

import org.omg.CORBA.Object;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.TreeSet;

/**
 * Created by omar on 06/04/17.
 */
public class DensePoly implements PolyInterface<Monome> {

    /**
     * Will store the Monome.
     * INVARIANT: will newer be null AND never contain the zero Monome
     */
    private TreeSet<Monome> elements = new TreeSet<>();

    /**
     * The zero DensePoly
     */
    public DensePoly() {}

    /**
     * The clone constructor
     * @param m the {@link DensePoly} to be copied, REQUIRED not null
     */
    public DensePoly(DensePoly m) {
        Objects.requireNonNull(m);

        this.elements = (TreeSet<Monome>) m.elements.clone();
    }

    /**
     * @param m A Poly-like object to be transformed into a DensePoly. REQUIRE not null
     */
    public DensePoly(PolyInterface<Monome> m) {
        Objects.requireNonNull(m);

        Iterator<Monome> it = m.iterator();
        while (it.hasNext()) {
            push(it.next());
        }
    }

    /**
     * Initialize a new Poly with only one Monome
     * @param coefficient the coefficient of the starting monome. REQUIRED not null
     * @param exponent    the exponent of the starting monome.    REQUIRED not null
     */
    public DensePoly(Integer coefficient, Integer exponent) {
        Objects.requireNonNull(coefficient);
        Objects.requireNonNull(exponent);

        push(new Monome(coefficient, exponent));
    }

    /**
     * @param exponent the wanted exponent
     * @return the Monome who has exponent `exponent` with coefficent equals to zero if not found
     */
    protected Monome safeFind(int exponent) {
        Monome m = new Monome(0, exponent);

        try {
            TreeSet<Monome> greater = (TreeSet<Monome>) this.elements.tailSet(m);
            Monome first = greater.first();

            if (first.getExponent().equals(m.getExponent())) {
                m = first;
            }
        } catch (NoSuchElementException e) {
        } finally {
            return m;
        }
    }

    /**
     * @return the degree of this Poly, that is the exponent of the highest Monome.
     *          Could be 0 if this is the *zero* Poly
     */
    public int degree() {
        if (isZero()) {
            return 0;
        }
        return this.elements.last().getExponent();
    }

    /**
     * @param n an exponent
     * @return the coefficient of the term in this that has exponent n; possibly 0
     */
    public int coefficient(int n) {
        return safeFind(n).getCoefficent();
    }

    /**
     * @param m A Monome to be added to this. REQUIRED not null
     * @return True if the element was added, false otherwise
     */
    protected boolean push(Monome m) {
        Objects.requireNonNull(m);

        // Don't push the "zero" monome
        if (m.isZero()) {
            return false;
        }
        return this.elements.add(m);
    }

    /**
     * @param p a Poly object to be added to this. REQUIRED not null
     * @return a new DensePoly that is this+p
     */
    public DensePoly add(PolyInterface<Monome> p) {
        Objects.requireNonNull(p);

        DensePoly res = new DensePoly(this);

        Iterator<Monome> it = p.iterator();
        while(it.hasNext()) {
            Monome m = it.next();
            res = res.add(m);
        }

        return res;
    }

    /**
     * This will compute this+(Monome m) in linear time
     *
     * @param m The monome to add. REQUIRED not null
     * @return a new DensePoly which is this+m
     */
    public DensePoly add(Monome m) {
        Objects.requireNonNull(m);

        DensePoly res = new DensePoly(this);

        // If we don't have a <Monome m> with the same degree of m, this will return a `0*x^n`
        // where n = m.getExponent()
        Monome t = res.safeFind(m.getExponent());

        assert t.sameDegree(m) : "(f, t :: Monomes) should have the same degree";

        res.elements.remove(m);
        res.push(t.add(m));

        return res;
    }

    /**
     * @return a new DensePoly that is this*(-1)
     */
    @Override
    public DensePoly minus() {
        DensePoly res = new DensePoly();

        Iterator<Monome> it = iterator();
        while (it.hasNext()) {
            Monome m = it.next();
            res = res.add(m.negate());
        }

        assert this.add(res).equals(new DensePoly()) : "this plus its negation should produce the zero poly";

        return res;
    }

    /**
     * @param p a Monome to be multiplied to this. REQUIRED not null
     * @return a new {@link DensePoly} that its this*p
     */
    public DensePoly mul(PolyInterface<Monome> p) {
        Objects.requireNonNull(p);

        DensePoly res = new DensePoly();
        Iterator<Monome> it = iterator();
        while (it.hasNext()) {
            Monome m = it.next();
            res = res.add(p.mul(m));
        }

        return res;
    }

    /**
     * @param m a Monome to be multiplied to this. REQUIRED not null
     * @return a new {@link DensePoly} that is this*({@link Monome} m)
     */
    public DensePoly mul(Monome m) {
        Objects.requireNonNull(m);

        DensePoly res = new DensePoly();
        Iterator<Monome> it = iterator();
        while (it.hasNext()) {
            Monome t = it.next();
            res.push(t.mul(m));
        }

        return res;
    }

    /**
     * @return True if this is the zero DensePoly, false otherwise
     */
    public boolean isZero() {
        // we can be the zero poly only if we have 0 elements.
        // Methods such as `push` guarantees us that a Monome with 0 as coefficient
        // will never be added to `this`
        return this.elements.size() == 0;
    }

    /**
     * @return an iterator to traverse the Monome inside of this
     */
    public Iterator<Monome> iterator() {
        return this.elements.iterator();
    }

    /**
     * @param a an object to be compored with this
     * @return True if a could be considered equals to this.
     *         Two DensePoly object are considered to be equals if they have the
     *         same Monome.
     * @todo this method could be even more generic: we can check the equality with any objects that implements PolyInterface
     */
    public boolean equals(Object a) {
        if (! (a instanceof DensePoly) || a == null) {
            return false;
        }
        DensePoly d = (DensePoly) a;
        return this.elements.equals(d.elements);
    }

    /**
     * @return a String representation of this
     */
    public String toString() {
        // if this is the zero DensePoly return the string representation of the zero Monome
        if (isZero()) {
            return (new Monome()).toString();
        }

        StringBuffer b = new StringBuffer();

        Iterator<Monome> it = iterator();
        while (it.hasNext()) {
            b.append(it.next().toString());
            b.append(' ');
        }

        return b.toString();
    }
}
