package it.uniud.dimi.Poly;

import java.util.Iterator;

/**
 * Created by omar on 06/04/17.
 */
public interface PolyInterface<Unit extends Monomial> {

    public int degree();

    public int coefficient(int n);

    public PolyInterface add(PolyInterface<Unit> p);

    public PolyInterface add(Unit p);

    public PolyInterface minus();

    public PolyInterface mul(PolyInterface<Unit> p);

    public PolyInterface mul(Unit p);

    public boolean isZero();

    public Iterator<Unit> iterator();
}
