package it.uniud.dimi.IntSet;

/**
 * This instance define all the method that make sense a Bag of
 * some type should have.
 *
 * It's parametric in order to be as generic as possible.
 */
public interface BagInterface<ElemType> {

    /**
     * Add an element to the bag
     * @param x the element to be added
     */
    public void add(ElemType x);

    /**
     * remove the oldest equal entry of the element x from the bag
     * @param x the element to be removed
     * @return true if the element is removed, false otherwise
     */
    public boolean remove(ElemType x);

    /**
     * Remove all the elements that are `equals' to x
     * @param x
     * @return true if one or more element were removed, false otherwise
     */
    public boolean removeAll(ElemType x);

    /**
     * Check if `x' is equals to at least one element of the bag
     * @param x the element to be searched
     * @return true if at least one element is equals to x
     */
    public boolean contains(ElemType x);

    /**
     * Get the number of elements in the bag that are equals to x
     * @param x the element to be searched
     * @return the number of elements that are equals to x
     */
    public int numberOfCopiesOf(ElemType x);

    /**
     * @return How bigger is the bag
     */
    public long size();

    /**
     * @return a random element from the bag
     * @throws EmptyIntSetException if the bag is empty
     */
    public ElemType choose() throws EmptyIntSetException;
}
