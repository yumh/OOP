package it.uniud.dimi.IntSet;

import it.uniud.dimi.Poly.Monome;

/**
 * Created by omar on 07/04/17.
 */
public class MonomeNotFoundException extends Exception {
    public MonomeNotFoundException() {
        super("That monome was not found");
    }

    public MonomeNotFoundException(Monome m) {
        super(String.format("The monome %s was not found", m.toString()));
    }
}
