package it.uniud.dimi.IntSet;

/**
 * Created by omar on 06/04/17.
 */
public class EmptyIntSetException extends Exception {
    public EmptyIntSetException() {
        super("The set is empty!");
    }
}
