package it.uniud.dimi.IntSet;

import java.util.*;

/**
 * This class provides an ATD for sets of int.
 * IntSet is mutable, unbounded.
 */
public class IntSet {

    /**
     * INVARIANT elements != null & elements contains no dublicates
     * & elements contains boxed int.
     * Elements is no sorted
     */
    protected Vector<Integer> elements;

    /**
     * Effect: initialize this to a new set, empty
     */
    public IntSet() {
        this.elements = new Vector<Integer>();
    }

    /**
     * Effect initialize this to a new set which contains each element of elts, duplicated element are not considered
     * @param elts REQUIRE not null
     */
    public IntSet(int[] elts) {
        elts = Objects.requireNonNull(elts);

        this.elements = new Vector<Integer>();

        for (int x: elts) {
            Integer y = new Integer(x);
            if (!this.elements.contains(y)) {
                this.elements.add(y);
            }
        }
    }

    /**
     * Copy constuctor.
     * Effect: initialize this to a new set that contains all and only the elements of s
     * @param s a set to be duplicated
     */
    @SuppressWarnings("unchecked")
    public IntSet(IntSet s) {
        Objects.requireNonNull(s);

        this.elements = (Vector<Integer>) s.elements.clone();
    }

    /**
     * MODIFY this: x is added to the set if x is not present
     */
    public void insert(int x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        Integer y = new Integer(x);
        if (!this.elements.contains(y)) {
            this.elements.add(y);
        }

        assert this.elements.contains(y) : "y should now be present in the set";
    }

    /**
     * MODIFY this: x is removed to this set if x is present
     *
     * @return true if x was removed
     */
    public boolean remove(int x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        Integer y = new Integer(x);
        boolean res = this.elements.remove(y);

        assert !this.elements.contains(y) : "y shouldn't now be present in the set";

        return res;
    }

    /**
     * @return true if x is present in this
     */
    public boolean isIn(int x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        Integer y = new Integer(x);
        int i = this.indexOf(y);
        boolean res = (i >= 0);

        assert !this.elements.contains(y) : "y shouldn't now be present in the set";

        return res;
    }

    /**
     * @param x
     * @return the index of x if it is present in this; -1 otherwise
     */
    private int indexOf(Integer x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        for (int i = 0; i < this.elements.size(); i++) {
            if (this.elements.get(i).equals(x)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * @return the number of elements in this
     */
    public long size() {
        assert (this.elements != null) : "this.elements mustn't be null";

        return this.elements.size();
    }

    /**
     * @return a random element in this
     * @throws EmptyIntSetException if this is emty
     */
    public int choose() throws EmptyIntSetException {
        assert (this.elements != null) : "this.elements mustn't be null";

        if (this.elements.isEmpty()) {
            throw new EmptyIntSetException();
        }

        Random randomGenerator = new Random();
        int x = randomGenerator.nextInt(this.elements.size());

        return this.elements.get(x);
    }

    /**
     * @param s2 REQUIRE not null
     * @return true if this and s2 contains the same set of int
     */
    public boolean sameValues(IntSet s2) {
        Objects.requireNonNull(s2);

        Collections.sort(this.elements); // BEWARE: integers are moved
        Collections.sort(s2.elements);

        return this.elements.equals(s2.elements);
    }
}
