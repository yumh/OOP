package it.uniud.dimi.IntSet;

/**
 * This class provides an ADT for a bag of int.
 */
public class IntBag extends GenericBag<Integer> {

    /** @inheritDoc */
    public IntBag() { super(); }

    /** @inheritDoc */
    public IntBag(Integer[] p) { super(p); }

    /** @inheritDoc */
    public IntBag(IntBag p) { super(p); }
}
