package it.uniud.dimi.IntSet;

import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

/**
 * A possible parametric implementation of BagInterface.
 *
 * All the equalities checks are performed against the `equals' method
 * of the stored objects.
 *
 * The elements are, as of now, stored inside a `Vector<>`,
 * so keep in mind this if you are interested in performance.
 */
public class GenericBag<ElemType> implements BagInterface<ElemType> {

    /**
     * INVARIANT elements != null
     * Elements are not sorted
     */
    protected Vector<ElemType> elements;

    /**
     * Initialize an empty bag
     */
    public GenericBag() {
        this.elements = new Vector<ElemType>();
    }

    /**
     * Create a bag with all the elements of p
     * @param p REQUIRED not null
     */
    public GenericBag(ElemType[] p) {
        this.elements = new Vector<ElemType>();

        for (ElemType a: p) {
            this.add(a);
        }
    }

    /**
     * Copy constuctor.
     * Effect: initialize this to a new set that contains all and only the elements of s
     * @param b a set to be duplicated
     */
    public GenericBag(GenericBag<ElemType> b) {
        this.elements = (Vector<ElemType>) b.elements.clone();
    }

    /**
     * @inheritDoc
     */
    public void add(ElemType x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        this.elements.add(x);

        assert this.elements.contains(x) : "x should now be present in the set";
    }

    /**
     * @inheritDoc
     */
    public boolean remove(ElemType x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        return this.elements.remove(x);
    }

    /**
     * @inheritDoc
     */
    public boolean removeAll(ElemType x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        if (!this.elements.contains(x)) {
            return false;
        }

        while (this.elements.contains(x)) {
            this.elements.remove(x);
        }

        assert !this.elements.contains(x) : "x should now not be present in the bag";
        return true;
    }

    /**
     * @inheritDoc
     */
    public boolean contains(ElemType x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        return this.elements.contains(x);
    }

    /**
     * @inheritDoc
     */
    public int numberOfCopiesOf(ElemType x) {
        assert (this.elements != null) : "this.elements mustn't be null";

        int result = 0;

        Iterator<ElemType> it = this.elements.iterator();
        while (it.hasNext()) {
            ElemType e = it.next();
            if (e.equals(x)) result++;
        }

        return result;
    }

    /**
     * @inheritDoc
     */
    public long size() {
        assert (this.elements != null) : "this.elements mustn't be null";

        return this.elements.size();
    }

    /**
     * @inheritDoc
     */
    public ElemType choose() throws EmptyIntSetException {
        assert (this.elements != null) : "this.elements mustn't be null";

        if (this.elements.size() == 0) {
            throw new EmptyIntSetException();
        }

        Random randomGenerator = new Random();
        int x = randomGenerator.nextInt(this.elements.size());
        return this.elements.get(x);
    }
}
