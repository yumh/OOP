package it.uniud.dimi;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

/**
 * This class provides an ADT for polynomials with integer
 * not null coefficents and with non negative exponents: c_0+c_1*x+c_2*x^2...
 * Poly is immutable, unbounded.
 * The empty poly is 0=0*x^0
 */
// public class Poly implements PolyInterface {
public class Poly {

    private class PolyIterator implements Iterator<Poly> {

        private List<PolynomialTerm> list;

        private int position = 0;

        public PolyIterator(List<PolynomialTerm> l) {
            this.list = l;
            this.position = 0;
        }

        public boolean hasNext() {
            return this.list.size() > this.position;
        }

        public Poly next() {
            PolynomialTerm p = this.list.get(this.position);

            position++;

            try {
                return new Poly(p.coeff, p.exponent);
            } catch (NegativeExponentException e) {
                assert false : "this should never happen";
                return null;
            }
        }
    }

    protected class PolynomialTerm implements Cloneable {
        int coeff;
        int exponent;

        PolynomialTerm(int c, int e) {
            this.coeff = c;
            this.exponent = e;
        }

        PolynomialTerm() {
            this.coeff = 0;
            this.exponent = 0;
        }

        /**
         * Copy constructor
         * @param ce: REQUIRE not null
         */
        PolynomialTerm(PolynomialTerm ce) {
            ce = Objects.requireNonNull(ce);
            this.coeff = ce.coeff;
            this.exponent = ce.exponent;
        }

        /**
         * check for equaliness
         *
         * Two PolynomialTerm are considered equals if they have the
         * same coeff and exponent
         */
        public boolean equals(Object b) {
            // if b is not at PolynomialTerm => false
            if (! (b instanceof PolynomialTerm)) {
                return false;
            }

            PolynomialTerm B = (PolynomialTerm) b;

            // special case: if they have both coeff equals to 0 they are equals,
            // no matter the exponent
            if (B.coeff == this.coeff && B.coeff == 0) {
                return true;
            }

            return B.exponent == this.exponent && B.coeff == this.coeff;
        }
    }

    /**
     * INVARIANT terms = the list of terms;
     * there is no relation between index of the term with exponent.
     * There can be terms with c=0
     */
    private Vector<PolynomialTerm> terms;

    /**
     * @return a new zero (empty) poly.
     */
    public Poly() {
        terms = new Vector<PolynomialTerm>();
    }

    /**
     * @param c: the coefficient
     * @param n: the exponent.
     * @return: a new Poly c*x^n if c!=0; otherwise the zero Poly
     * @throws; NegativeExponentException when n<0
     */
    public Poly(int c, int n) throws NegativeExponentException {
        if (n < 0) {
            throw new NegativeExponentException();
        }

        terms = new Vector<PolynomialTerm>();

        if (c!=0) {
            PolynomialTerm ce = new PolynomialTerm(c, n);
            terms.addElement(ce);
        }
    }

    /**
     * Make a deep copy of the given Poly p
     * @param p: REQUIRE not null
     */
    public Poly(Poly p) {
        Objects.requireNonNull(p);

        this.terms = new Vector<>();

        Iterator<PolynomialTerm> it = p.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm t = it.next();
            this.terms.add(new PolynomialTerm(t));
        }
    }

    /**
     * @param p: the poly to be added to this; REQUIRE not null
     * @return a new poly that is this+p
     */
    public Poly add(Poly p) {
        Poly result = new Poly(this);
        Iterator<PolynomialTerm> it = p.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm t = it.next();
            result = result.add(t);
        }
        return result;
    }

    /**
     * add a monome to an existing polynome
     * @param p the given monome
     * @return a new Poly equals to this+p
     */
    protected Poly add(PolynomialTerm p) {
        Poly result = new Poly(this);
        Iterator<PolynomialTerm> it = result.terms.iterator();

        while (it.hasNext()) {
            PolynomialTerm t = it.next();
            if (p.exponent == t.exponent) {
                t.coeff += p.coeff;
                return result;
            }
        }

        result.terms.add(p);
        return result;
    }

    /**
     * @return the largest exponent in this with non zero coeff or 0 if this is the zero poly
     */
    public int degree() {
        assert (this.terms != null) : "this.terms mustn't be null";

        if (this.terms.isEmpty()) {
            return 0;
        }

        int highest = 0;

        Iterator<PolynomialTerm> it = this.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm cpe = it.next();
            if (cpe.exponent > highest) {
                highest = cpe.exponent;
            }
        }

        return highest;
    }

    /**
     * @param n an exponent
     * @return the coefficent of the term in this
     *      that has exponent n; possibly 0
     */
    public int coefficient(int n) {
        assert (this.terms != null) : "this.terms mustn't be null";

        Iterator<PolynomialTerm> it = this.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm cpe = it.next();
            if (cpe.exponent == n) {
                return cpe.coeff;
            }
        }

        return 0;
    }

    /**
     * @return the poly minus -(this)
     */
    public Poly minus() {
        Poly result = new Poly(this);
        Iterator<PolynomialTerm> it = result.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm t = it.next();
            t.coeff *= -1;
        }
        return result;
    }

    /**
     * @param p the poly to be multiplied to this;
     *          REQUIRE not null
     *
     * @return a new poly that is this*p
     */
    public Poly mul(Poly p) {
        Poly result = new Poly();
        Iterator<PolynomialTerm> it = this.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm cur = it.next();
            result = result.add(p.mul(cur));
        }

        return result;
    }

    /**
     * @param t a polynomial term to be moltiplied to this
     * @return a new poly that is this * k
     */
    protected Poly mul(PolynomialTerm t) {
        Poly result = new Poly(this);
        Iterator<PolynomialTerm> it = result.terms.iterator();

        while (it.hasNext()) {
            PolynomialTerm p = it.next();

            p.coeff    *= t.coeff;
            p.exponent += t.exponent;
        }

        return result;
    }

    /**
     * @return true if this is the zero Poly, false otherwise
     */
    public boolean isZero() {
        if (this.terms.size() == 0) {
            return true;
        }

        if (this.terms.size() == 1 && this.terms.get(0).equals(new PolynomialTerm())) {
            return true;
        }

        return false;
    }

    /**
     * Check if this equals another Poly
     * Two Poly are considered equals if they have the same addends,
     * no matter the order
     *
     * @param B a Poly to check if it's equals to this, REQUIRE not null
     * @return true if this equals b
     */
    public boolean equals(Object B) {
        Objects.requireNonNull(B);

        if (! (B instanceof Poly)) return false;

        Poly b = (Poly) B;

        if (this.isZero() && b.isZero()) {
            return true;
        }

        if (this.isZero() && !b.isZero() || !this.isZero() && b.isZero()) {
            return false;
        }

        Iterator<PolynomialTerm> it = this.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm t = it.next();

            if (t.exponent == 0) continue;

            if (! b.terms.contains(t)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        assert (this.terms != null) : "this.terms mustn't be null";

        if (this.terms.size() == 0) {
            return "+0";
        }

        StringBuffer s = new StringBuffer();
        Iterator<PolynomialTerm> it = this.terms.iterator();
        while (it.hasNext()) {
            PolynomialTerm cpe = it.next();
            s.append(String.format("+%d*x^%d", cpe.coeff, cpe.exponent));
        }

        return s.toString();
    }

    public Iterator<Poly> iterator() {
        return new PolyIterator(this.terms);
    }

}
