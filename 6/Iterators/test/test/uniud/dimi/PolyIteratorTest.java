package test.uniud.dimi;

import it.uniud.dimi.NegativeExponentException;
import it.uniud.dimi.Poly;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

/**
 * Created by omar on 12/04/17.
 */
public class PolyIteratorTest {
    @Test
    void iteratorTest() throws NegativeExponentException {
        Poly a = new Poly(5, 7);
        Poly b = new Poly(4, 2);
        Poly c = new Poly(4, 1);

        Poly result = a.add(b).add(c);

        Iterator<Poly> it = result.iterator();
        while (it.hasNext()) {
            Poly t = it.next();
            System.out.format("The current poly is %s%n", t.toString());
        }
    }
}
