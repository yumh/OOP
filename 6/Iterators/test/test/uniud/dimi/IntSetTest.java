package test.uniud.dimi;

import it.uniud.dimi.IntSet;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Random;

/**
 * Created by omar on 12/04/17.
 */
public class IntSetTest {

    @Test
    void iteratorTest() {
        IntSet set = new IntSet();

        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            set.insert(r.nextInt());
        }

        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            System.out.format("%d%n", it.next());
        }
    }
}
