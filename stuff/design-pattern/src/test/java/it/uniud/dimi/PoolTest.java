package it.uniud.dimi;

import it.uniud.dimi.Monomial.MonomialInterface;
import it.uniud.dimi.Monomial.MonomialPool;
import it.uniud.dimi.Polynomial.Pair;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.*;

/**
 * Created by omar on 12/04/17.
 */
public class PoolTest extends MonomialPool<Integer> {

    public PoolTest() {
        super(0);
    }

    @Test
    public void shouldBuildTheSameMonomial() {
        this.table = new HashMap<Pair<Integer>, MonomialInterface<Integer>>();

        assertEquals(this.table.size(), 0);

        MonomialInterface m = this.get();
        assertEquals(this.table.size(), 1);

        assertTrue(this.get() == m);
        assertEquals(this.table.size(), 1);

        MonomialInterface n = this.get(3, 4);
        assertEquals(this.table.size(), 2);
        assertTrue(n == this.get(3, 4));
    }
}
