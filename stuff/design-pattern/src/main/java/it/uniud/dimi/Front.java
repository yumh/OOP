package it.uniud.dimi;

import it.uniud.dimi.Monomial.Monomial;
import it.uniud.dimi.Monomial.MonomialPool;
import it.uniud.dimi.Polynomial.Poly;
import it.uniud.dimi.Polynomial.PolyInterface;

/**
 * Created by omar on 12/04/17.
 */
public class Front {

    MonomialPool<Integer> proxy = new MonomialPool<>();

    public Front() {
        // something
    }

    /**
     * @return the zero Poly
     */
    public PolyInterface<Integer, Monomial<Integer>> initializePoly() {
        // TODO: the poly should use the MonomialPool class to construct new Monomial, but how?
        // proposals:
        //  - inject a MonomialPool as constructor parameter
        //  - hardcoding the MonomialPool class inside Poly
        //  - some sort of DI
        //  - ?
        return new Poly<Integer, Monomial>(this.proxy);
    }

}
