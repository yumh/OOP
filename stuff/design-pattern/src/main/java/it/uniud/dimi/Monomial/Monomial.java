package it.uniud.dimi.Monomial;

import java.util.Objects;

/**
 * Created by omar on 11/04/17.
 */
public class Monomial<N extends Number> extends AbstractMonomial<N> {

    /**
     * Store the coefficient
     *
     * INVARIANT: never be null
     */
    N coefficient;

    /**
     * Store the exponent
     *
     * INVARIANT: never be null
     */
    N exponent;

    /**
     * Build a new monomial object with given exponent & coefficient
     * @param coefficient the given coefficient
     * @param exponent    the given exponent
     */
    public Monomial(N coefficient, N exponent) {
        this.coefficient = coefficient;
        this.exponent    = exponent;
    }

    /**
     * {@inheritDoc}
     */
    public N getCoefficient() {
        assert this.coefficient != null : "coefficient mustn't be null";
        assert this.exponent != null : "exponent mustn't be null";

        return this.coefficient;
    }

    /**
     * {@inheritDoc}
     */
    public N getExponent() {
        assert this.coefficient != null : "coefficient mustn't be null";
        assert this.exponent != null : "exponent mustn't be null";

        return this.exponent;
    }

    /**
     * {@inheritDoc}
     */
    public MonomialInterface add(MonomialInterface addend) throws IllegalArgumentException {
        assert this.coefficient != null : "coefficient mustn't be null";
        assert this.exponent != null : "exponent mustn't be null";

        if (! this.getExponent().equals(addend.getExponent())) {
            throw new IllegalArgumentException(
                    String.format("addend (%s) does not have the same degree as this (%s)", addend.toString(), this.toString())
            );
        }

        int newCoefficient = addend.getCoefficient().intValue() + this.getCoefficient().intValue();
        return new Monomial(newCoefficient, getExponent());
    }

    /**
     * {@inheritDoc}
     */
    public MonomialInterface mul(MonomialInterface factor) {
        assert this.coefficient != null : "coefficient mustn't be null";
        assert this.exponent != null : "exponent mustn't be null";

        int newCoefficient = this.getCoefficient().intValue() * factor.getCoefficient().intValue();
        int newExponent = this.getExponent().intValue() + factor.getExponent().intValue();
        return new Monomial(newCoefficient, newExponent);
    }

    /**
     * @param o a {@link MonomialInterface} to be compared with this. REQUIRE not null
     * @return -1 if this < o, 0 if they are equals, 1 if this > o
     */
    public int compareTo(MonomialInterface<N> o) {
        Objects.requireNonNull(o);

        // special case
        if (this.isZero() && o.isZero()) {
            return 0;
        }

        Integer thisExponent    = this.getExponent().intValue();
        Integer thisCoefficient = this.getCoefficient().intValue();

        Integer oExponent    = o.getExponent().intValue();
        Integer oCoefficient = o.getCoefficient().intValue();

        if (thisExponent == oExponent && thisCoefficient == oCoefficient) {
            return 0;
        } else if (thisExponent < oExponent) {
            return -1;
        } else {
            return +1;
        }
    }
}
