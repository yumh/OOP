package it.uniud.dimi.Monomial;

import it.uniud.dimi.Polynomial.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * The mission if this class is to store the calculated monomial and return/initialize them when asked
 */
public class MonomialPool<N extends Number> {

    Logger logger = LogManager.getLogger(MonomialPool.class);

    /**
     * store the HashMap where the monomials will be added
     *
     * INVARIANT: this will never be null
     */
    protected Map<Pair<N>, MonomialInterface<N>> table = new HashMap<>();

    /**
     * store the `zero' value for the given type N
     *
     * INVARIANT: this will never be null
     */
    protected N zero = (N) Integer.valueOf(0);

    /**
     * @param coefficient the coefficient of the wanted monomial
     * @param exponent    the exponent of the wanted monomial
     * @return a monomial with the given coefficient and exponent
     */
    public MonomialInterface<N> get(N coefficient, N exponent) {
        Pair<N> pair = new Pair<>(coefficient, exponent);

        // if the monomial was already initialized, simply return it
        if (this.table.containsKey(pair)) {
            logger.info("cache hit");
            return this.table.get(pair);
        }

        logger.info("cache miss");

        // otherwise initialize it
        MonomialInterface<N> monomial = new Monomial<>(coefficient, exponent);

        // add it to the table
        this.table.put(pair, monomial);

        // and return it
        return monomial;
    }

    /**
     * @return the zero monomial
     */
    public MonomialInterface get() {
        return this.get(zero, zero);
    }
}
