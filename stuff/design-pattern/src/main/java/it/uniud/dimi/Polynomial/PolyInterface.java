package it.uniud.dimi.Polynomial;

import it.uniud.dimi.Monomial.MonomialInterface;

import java.util.Iterator;

/**
 * Created by omar on 12/04/17.
 */
public interface PolyInterface<Num extends Number, Mon extends MonomialInterface<Num>> {

    /**
     * @return an iterator of {@link MonomialInterface}
     */
    public Iterator<Mon> iterator();

    /**
     * @return the degree of this Poly, that is the exponent of the highest monomial.
     *         Could be 0 if this is the *zero* Poly
     */
    public Num degree();

    /**
     * @param coefficient a Number
     * @return the {@link MonomialInterface} inside this with `coefficient' as coefficient
     *          NOTE: return the zero {@link MonomialInterface} with the given `coefficient' if not found
     */
    public Num coefficient(Num coefficient);

    /**
     * @param p another {@link PolyInterface}
     * @return a new {@link PolyInterface} which is this plus p
     */
    public PolyInterface<Num, Mon> add(PolyInterface<Num, Mon> p);

    /**
     * @param p another {@link MonomialInterface}
     * @return a new {@link PolyInterface} which is this plus p
     */
    public PolyInterface<Num, Mon> add(Mon p);

    /**
     * @param p another {@link PolyInterface}
     * @return a new {@link PolyInterface} which is this times p
     */
    public PolyInterface<Num, Mon> mul(PolyInterface<Num, Mon> p);

    /**
     * @param p another {@link MonomialInterface}
     * @return a new {@link PolyInterface} which is this times p
     */
    public PolyInterface<Num, Mon> mul(Mon p);

    /**
     * @return true if this is the zero poly
     */
    public boolean isZero();
}
