package it.uniud.dimi.Polynomial;

import it.uniud.dimi.Monomial.MonomialInterface;
import it.uniud.dimi.Monomial.MonomialPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.TreeSet;

/**
 * Created by omar on 12/04/17.
 */
public class Poly<Mon extends MonomialInterface<Integer>> implements PolyInterface {

    /**
     * INVARIANT: this will never be null
     */
    protected TreeSet<Mon> elements = new TreeSet<>();

    /**
     * an pool where all the instances of monomials are stored
     *
     * INVARIANT: never be null
     */
    protected MonomialPool<Integer> pool;
    // protected MonomialPool<Mon<N>> pool;

    /**
     * Store the logger for this class
     */
    Logger logger = LogManager.getLogger(Poly.class);

    /**
     * @param pool a pool where all the monomials are stored
     */
    public Poly(MonomialPool pool) {
        this.pool = pool;
    }

    /**
     * Build a Poly with only one Monomial inside, with the given coefficient and exponent
     * @param pool a pool
     * @param coefficient
     * @param exponent
     */
    public Poly(MonomialPool<Integer> pool, Integer coefficient, Integer exponent) {
        this.pool = pool;

        push(pool.get(coefficient, exponent));
    }

    /**
     * Push an element to the `elements' if it's not zero
     * @param m REQUIRED not null
     */
    protected void push(MonomialInterface<Integer> m) {
        Objects.requireNonNull(m);

        if (!m.isZero()) {
            this.elements.add((Mon) m);
        }

        logger.info("the given element was zero, ignoring", m);
    }

    /**
     * {@inheritDoc}
     */
    public Iterator<Mon> iterator() {
        return this.elements.iterator();
    }

    /**
     * {@inheritDoc}
     */
    public Number degree() {
        if (this.isZero()) {
            return 0;
        }
        return this.elements.last().getExponent();
    }

    /**
     * {@inheritDoc}
     */
    public Number coefficient(Number coefficient) {
        return null;
    }

    /**
     * @param exponent the wanted exponent
     * @return the Monome who has exponent `exponent` with coefficent equals to zero if not found
     */
    protected Mon safeFind(Integer exponent) {
        Iterator<Mon> it = this.iterator();

        while (it.hasNext()) {
            Mon m = it.next();
            if (m.getExponent().equals(exponent)) {
                return m;
            }
        }

        return (Mon) pool.get();
    }

    /**
     * {@inheritDoc}
     */
    public PolyInterface add(PolyInterface p) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public PolyInterface add(MonomialInterface p) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public PolyInterface mul(PolyInterface p) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public PolyInterface mul(MonomialInterface p) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isZero() {
        return false;
    }
}
