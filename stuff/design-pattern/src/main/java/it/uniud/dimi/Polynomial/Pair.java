package it.uniud.dimi.Polynomial;

/**
 * This class implements an immutable parametric pair of two element of the same type
 */
public class Pair<A> {
    protected A first;

    protected A second;

    public Pair(A f, A s) {
        first = f;
        second = s;
    }

    public boolean equals(Object a) {
        if (a == null) {
            return false;
        }

        try {
            Pair<A> aa = (Pair<A>) a;
            return this.first.equals(aa.first) && this.second.equals(aa.second);
        } catch (Exception e) {
            return false;
        }
    }

    public int hashCode() {
        return String.format("(%s, %s)", this.first.toString(), this.second.toString()).hashCode();
    }
}
