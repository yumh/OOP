package it.uniud.dimi.Monomial;

/**
 * Immutable representation of a monomial
 */
public interface MonomialInterface<N extends Number> extends Comparable<MonomialInterface<N>> {

    /**
     * @return the coefficient
     */
    public N getCoefficient();

    /**
     * @return the exponent
     */
    public N getExponent();

    /**
     * @param addend a {@link MonomialInterface} to be added to this. REQUIRED not null
     * @return a new {@link MonomialInterface} that is this+a
     * @throws IllegalArgumentException if the addend does not have the same degree as this
     */
    public MonomialInterface<N> add(MonomialInterface addend) throws IllegalArgumentException;

    /**
     * @param factor a {@link MonomialInterface} to be multiplied to this. REQUIRED not null
     * @return a new {@link MonomialInterface} that is this+a
     */
    public MonomialInterface<N> mul(MonomialInterface factor);

    /**
     * @return true if this is the zero monomial
     */
    public boolean isZero();
}
