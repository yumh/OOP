package it.uniud.dimi.Monomial;

/**
 * Created by omar on 11/04/17.
 */
public abstract class AbstractMonomial<N extends Number> implements MonomialInterface<N> {

    /**
     * {@inheritDoc}
     */
    public boolean isZero() {
        return this.getCoefficient().equals(0);
    }

    /**
     * @param a an object, even null
     * @return True if a is an instanceOf {@link MonomialInterface} and have equals exponents AND exponents
     *          they are considered to be equals also if they are both zero, regardless of the exponent
     */
    public boolean equals(Object a) {
        if (a == null || !(a instanceof MonomialInterface)) {
            return false;
        }

        MonomialInterface A = (MonomialInterface) a;
        if (A.isZero() && this.isZero()) {
            return true;
        }

        return A.getCoefficient().equals(this.getCoefficient())
                && A.getExponent().equals(this.getExponent());
    }

    /**
     * @return a string that represent this
     */
    public String toString() {
        if (this.isZero()) {
            return "0";
        }

        return String.format("%d*x^%d", this.getCoefficient(), this.getExponent());
    }
}
